﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRWebRole.Models
{
    public class Job: TableEntity
    {
        public Job (string lastName, string firstName)
	    {
             this.PartitionKey = lastName;
             this.RowKey = firstName;
             this.Status = "Submitted";   
	    }


         public Job() { }

         public string Status { get; set; }

         
    }
}