﻿using Microsoft.Azure;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FRWebRole.Controllers
{
    public class DownloadController : Controller
    {
        [HttpGet]
        public FileResult DownloadFile(string id)
        {
            //id = id + ".jpg";
            var CurrentFileName = DownloadBlob(id);

            string contentType = "application/octet-stream";

            if (CurrentFileName.Contains(".jpg"))
            {
                contentType = "application/jpg";
            }
            else if (CurrentFileName.Contains(".png"))
            {
                contentType = "application/png";
            }
            //else if (CurrentFileName.Contains(".prt"))
            //{
            //    contentType = "application/prt";
            //}
            //else if (CurrentFileName.Contains(".CATPart"))
            //{
            //    contentType = "application/CATPart";
            //}
            var fresult = File(CurrentFileName, contentType);
            fresult.FileDownloadName = id;
            return fresult;
           // return File(CurrentFileName, contentType);
        }
        private string DownloadBlob(string filename)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("DBConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("output");

            // Retrieve reference to a blob named "photo1.jpg".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
            var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), filename);
            // Save blob contents to a file.
            using (var fileStream = System.IO.File.OpenWrite(path))
            {
                blockBlob.DownloadToStream(fileStream);
            }
            return path;
        }

        
    }
}