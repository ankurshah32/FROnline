﻿using FRWebRole.Models;
using Microsoft.Azure;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FRWebRole.Controllers
{

    public class UploadController : Controller
    {
        private void UploadBlob(string filename)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("DBConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

            // Retrieve reference to a blob named "myblob".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
            var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), filename);
            // Create or overwrite the "myblob" blob with contents from a local file.
            
            using (var fileStream = System.IO.File.OpenRead(path))
            {
                blockBlob.UploadFromStream(fileStream);
            }
            Job job = new Job(filename, filename);
            InsertTableEntry(job);
            SendMessage(filename);
        }
        private void InsertTableEntry(Job job)
        {

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("DBConnectionString"));

            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            // Create the table if it doesn't exist.
            CloudTable table = tableClient.GetTableReference("Jobs");
            table.CreateIfNotExists();
            TableOperation insertOperation = TableOperation.Insert(job);

            // Execute the insert operation.
            table.Execute(insertOperation);
        }

        private void SendMessage(string msg)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                    CloudConfigurationManager.GetSetting("DBConnectionString"));
            // Create the queue client
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue queue = queueClient.GetQueueReference("myqueue");

            // Create the queue if it doesn't already exist
            queue.CreateIfNotExists();

            CloudQueueMessage message = new CloudQueueMessage(msg);
            queue.AddMessage(message);
        }
        // GET: UploadFile
        public ActionResult UploadFile()
        {
            ViewBag.UPLOADED = false;
            return View();
        }
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {

            if (file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                string uuid = Guid.NewGuid().ToString();
                var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), uuid);
                file.SaveAs(path);
                UploadBlob(uuid);
                ViewBag.UPLOADED = true;
                ViewBag.FileName = uuid;
            }
            return View("UploadFile");
        }
    }
}